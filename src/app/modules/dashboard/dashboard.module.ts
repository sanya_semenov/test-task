import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserListComponent } from './user-list/user-list.component';
import { FilterPanelComponent } from './filter-panel/filter-panel.component';
import { HttpService } from '../../shared/http.service';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    PipesModule
  ],
  declarations: [
    UserListComponent,
    FilterPanelComponent
  ],
  providers: [
    HttpService
  ],
  exports: [
    UserListComponent,
    FilterPanelComponent
  ]
})
export class DashboardModule { }

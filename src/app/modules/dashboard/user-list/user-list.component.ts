import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { HttpService } from '../../../shared/http.service';
import { UserView } from '../../../models/user-view.model';
import { ShareService } from '../../../shared/share.service';
import { Subscription } from 'rxjs';
import { FilterItem } from '../../../models/filter-item.model';
import { distinctUntilChanged } from 'rxjs/operators';
import { CityFilterPipe } from '../../../pipes/city-filter/city-filter.pipe';
import { SortObject } from '../../../models/sort-object.model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.less']
})
export class UserListComponent implements OnInit, OnDestroy {

  constructor(
    private httpService: HttpService,
    private shareService: ShareService
  ) {
    this.windowWidth = window.innerWidth;
  }
  windowWidth: number;
  userList: UserView[];
  filteredList: UserView[];
  isDescending: boolean[];
  keysToFilter: string[];
  genderValues: FilterItem[];
  departmentValues: FilterItem[];
  cityValues: FilterItem[];

  currentSortObj: SortObject;
  numberSortObj = new SortObject('number', false);
  nameSortObj = new SortObject('name', false);
  ageSortObj = new SortObject('age', false);
  genderSortObj = new SortObject('gender', false);
  departmentSortObj = new SortObject('department', false);
  addressSortObj = new SortObject('fullAddress', false);

  genderSubscription: Subscription;
  departmentSubscription: Subscription;
  citySubscription: Subscription;
  filteredListSubscription: Subscription;

  @HostListener('window:resize', ['$event']) onWindowResize(event) {
    this.windowWidth = window.innerWidth;
  }

  ngOnInit() {
    this.httpService.getUserList().subscribe((data: any) => {
      this.userList = data.map((x, index) => {
        const user = new UserView(x);
        user.number = ++index;
        return user;
      });
      this.filteredList = this.userList;
      this.shareService.userList.next(this.userList);
      if (this.userList.length > 1) {
        this.isDescending = Object.keys(this.userList[0]).map(x => false);
      }

      this.FormValuesLists();

      console.log('user list: ', this.userList);
    });

    this.filteredListSubscription = this.shareService.filteredList.subscribe((data: UserView[]) => {
      this.filteredList = data;
    });
  }

  FormValuesLists() {
    this.genderValues = [
      new FilterItem('male', false),
      new FilterItem('female', false)
    ];
    // this.genderValues = this.GetUniqueValues(this.userList.map(x => x.gender));
    this.departmentValues = this.GetUniqueValues(this.userList.map(x => x.department));
    this.cityValues = this.GetUniqueValues(this.userList.map(x => x.city));


    this.shareService.sendCities(this.cityValues);
    this.shareService.sendDepartments(this.departmentValues);
    this.shareService.sendGenders(this.genderValues);

    this.citySubscription = this.shareService.cityValues_FromFilter.subscribe((data: FilterItem[]) => {
      this.cityValues = data;
    });
    this.departmentSubscription = this.shareService.departmentValues_FromFilter.subscribe((data: FilterItem[]) => {
      this.departmentValues = data;
    });
    this.genderSubscription = this.shareService.genderValues_FromFilter.subscribe((data: FilterItem[]) => {
      this.genderValues = data;
    });
  }

  arraysEqual(arr1, arr2) {
    if (arr1.length !== arr2.length) {
      return false;
    }
    for (let i = 0; i < arr1.length; i++) {
      if (arr1[i] !== arr2[i]) {
        return false;
      }
    }

    return true;
  }

  GetUniqueValues(list) {
    return list.filter((value, i, arr) => arr.indexOf(value) === i).map(x => new FilterItem(x, false));
  }

  ToggleSort(sortObj) {
    sortObj.isDescending = !sortObj.isDescending;
    if (this.currentSortObj) {
      this.currentSortObj.isTouched = false;
    }
    if (!sortObj.isTouched) {
      sortObj.isTouched = true;
    }
    // this.currentSortObj = null;
    this.currentSortObj = sortObj;
  }

  ngOnDestroy(): void {
    this.departmentSubscription.unsubscribe();
    this.citySubscription.unsubscribe();
    this.genderSubscription.unsubscribe();
    this.filteredListSubscription.unsubscribe();
  }
}

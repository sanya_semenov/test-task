import { Component, OnInit, OnDestroy } from '@angular/core';
import { ShareService } from '../../../shared/share.service';
import { Subscription } from 'rxjs';
import { FilterItem } from '../../../models/filter-item.model';

import { distinctUntilChanged } from 'rxjs/operators';
import { ShareList } from '../../../models/share-list.model';
import { GenderFilterPipe } from '../../../pipes/gender-filter/gender-filter.pipe';
import { DepartmentFilterPipe } from '../../../pipes/department-filter/department-filter.pipe';
import { CityFilterPipe } from '../../../pipes/city-filter/city-filter.pipe';
import { UserView } from '../../../models/user-view.model';

@Component({
  selector: 'app-filter-panel',
  templateUrl: './filter-panel.component.html',
  styleUrls: ['./filter-panel.component.less']
})
export class FilterPanelComponent implements OnInit, OnDestroy {
  constructor(
    private shareService: ShareService,
    private genderFilter: GenderFilterPipe,
    private departmentFilter: DepartmentFilterPipe,
    private cityFilter: CityFilterPipe
  ) { }

  genderList = { isOpen: false };
  departmentList = { isOpen: false };
  cityList = { isOpen: false };

  userList: UserView[];
  filteredList: UserView[];

  citySubscription: Subscription;
  departmentSubscription: Subscription;
  genderSubscription: Subscription;
  userListSubscription: Subscription;

  _cityValues: FilterItem[];
  get cityValues() {
    return this._cityValues;
  }
  set cityValues(value) {
    this._cityValues = value;
  }

  _departmentValues: FilterItem[];
  get departmentValues() {
    return this._departmentValues;
  }
  set departmentValues(value) {
    this._departmentValues = value;
  }

  _genderValues: FilterItem[];
  get genderValues() {
    return this._genderValues;
  }
  set genderValues(value) {
    this._genderValues = value;
  }


  ngOnInit(): void {
    this.userListSubscription = this.shareService.userList.subscribe((data: UserView[]) => {
      this.userList = data;
      this.filteredList = this.userList;
    });
    this.citySubscription = this.shareService.cityValues_ToFilter.subscribe((data: FilterItem[]) => {
      this.cityValues = data;
      this.FindCityMatches();
    });
    this.departmentSubscription = this.shareService.departmentValues_ToFilter.subscribe((data: FilterItem[]) => {
      this.departmentValues = data;
      this.FindDepartmentMatches();
    });
    this.genderSubscription = this.shareService.genderValues_ToFilter.subscribe((data: FilterItem[]) => {
      this.genderValues = data;
      this.FindGenderMatches();
    });
  }

  ToggleList(list) {
    list.isOpen = !list.isOpen;
  }

  ToggleCheck(item) {
    if (item) {
      item.isChecked = !item.isChecked;
      this.shareService.filterCities(this.cityValues);
      this.shareService.filterDepartments(this.departmentValues);
      this.shareService.filterGenders(this.genderValues);
      // console.log('filtered ', this.filteredList);
      this.filteredList = this.userList;
      this.filteredList = this.genderFilter.transform(
        this.departmentFilter.transform(
          this.cityFilter.transform(this.userList, this.cityValues), this.departmentValues), this.genderValues);
      this.shareService.filteredList.next(this.filteredList);
      this.FindAllMatches();
    }
  }

  FindAllMatches() {
    this.FindCityMatches();
    this.FindDepartmentMatches();
    this.FindGenderMatches();
  }

  FindCityMatches() {
    this.cityValues.forEach(item => {
      const filtered = this.cityFilter.transform(this.filteredList, [new FilterItem(item.value, true)]);
      const filteredCount = filtered ? filtered.length : 0;
      item.matches = filteredCount;
    });
    console.log('city values', this.cityValues);
  }

  FindDepartmentMatches() {
    this.departmentValues.forEach(item => {
      const filtered = this.departmentFilter.transform(this.filteredList, [new FilterItem(item.value, true)]);
      const filteredCount = filtered ? filtered.length : 0;
      item.matches = filteredCount;
    });
    console.log('department values', this.departmentValues);
  }

  FindGenderMatches() {
    this.genderValues.forEach(item => {
      const filtered = this.genderFilter.transform(this.filteredList, [new FilterItem(item.value, true)]);
      const filteredCount = filtered ? filtered.length : 0;
      item.matches = filteredCount;
    });
    console.log('gender values', this.genderValues);
  }

  ngOnDestroy(): void {
    this.departmentSubscription.unsubscribe();
    this.citySubscription.unsubscribe();
    this.genderSubscription.unsubscribe();
  }
}

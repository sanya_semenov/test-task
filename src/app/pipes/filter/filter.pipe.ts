import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../models/user.model';
import { FilterItem } from '../../models/filter-item.model';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(list: User[], genderValues: FilterItem[], departmentValues: FilterItem[], cityValues: FilterItem[]): any {
    // if (!this.hasToBeFiltered(genderValues, departmentValues, cityValues)) {
    //   return list;
    // }
    return list;
  }

  // hasToBeFiltered(genderValues: FilterItem[], departmentValues: FilterItem[], cityValues: FilterItem[]) {
  //   return genderValues.some(x => x.isChecked === true) ||
  //     departmentValues.some(x => x.isChecked === true) ||
  //     cityValues.some(x => x.isChecked === true);
  // }

}

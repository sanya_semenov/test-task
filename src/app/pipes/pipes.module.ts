import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterPipe } from './filter/filter.pipe';
import { SortPipe } from './sort/sort.pipe';
import { GenderFilterPipe } from './gender-filter/gender-filter.pipe';
import { DepartmentFilterPipe } from './department-filter/department-filter.pipe';
import { CityFilterPipe } from './city-filter/city-filter.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    FilterPipe,
    SortPipe,
    GenderFilterPipe,
    DepartmentFilterPipe,
    CityFilterPipe
  ],
  exports: [
    FilterPipe,
    SortPipe,
    GenderFilterPipe,
    DepartmentFilterPipe,
    CityFilterPipe
  ],
  providers: [
    GenderFilterPipe,
    DepartmentFilterPipe,
    CityFilterPipe
  ]
})
export class PipesModule { }

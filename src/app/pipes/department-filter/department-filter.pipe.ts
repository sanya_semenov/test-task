import { Pipe, PipeTransform } from '@angular/core';
import { UserView } from '../../models/user-view.model';
import { FilterItem } from '../../models/filter-item.model';

@Pipe({
  name: 'departmentFilter'
})
export class DepartmentFilterPipe implements PipeTransform {

  transform(list: UserView[], departmentValues: FilterItem[]): any {
    if (!this.hasToBeFiltered(departmentValues)) {
      return list;
    } else if (list) {
      return list.filter(item =>
        (departmentValues.filter(x => x.isChecked).map(x => x.value)).indexOf(item.department) > -1
      );
    }
    return [];
  }

  hasToBeFiltered(departmentValues): boolean {
    return departmentValues && departmentValues.some(x => x.isChecked);
  }

}

import { Pipe, PipeTransform } from '@angular/core';
import { UserView } from '../../models/user-view.model';
import { FilterItem } from '../../models/filter-item.model';

@Pipe({
  name: 'genderFilter'
})
export class GenderFilterPipe implements PipeTransform {

  transform(list: UserView[], genderValues: FilterItem[]): any {
    if (!this.hasToBeFiltered(genderValues)) {
      return list;
    } else if (list) {
      return list.filter(item =>
        (genderValues.filter(x => x.isChecked).map(x => x.value)).indexOf(item.gender) > -1
      );
    }
    return [];
  }

  hasToBeFiltered(genderValues): boolean {
    return genderValues && genderValues.some(x => x.isChecked);
  }

}

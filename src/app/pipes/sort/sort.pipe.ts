import { Pipe, PipeTransform } from '@angular/core';
import { UserView } from '../../models/user-view.model';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(list: UserView[], key: string, isDescending: boolean): any {
    if (!key) {
      return list;
    }
    const factor = isDescending ? 1 : -1;
    return list.sort((a, b) => {
      if (a[key] < b[key]) {
        return -1 * factor;
      }
      if (a[key] > b[key]) {
        return 1 * factor;
      }
      return 0;
    });
  }

}

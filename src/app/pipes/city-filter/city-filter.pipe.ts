import { Pipe, PipeTransform } from '@angular/core';
import { FilterItem } from '../../models/filter-item.model';
import { UserView } from '../../models/user-view.model';

@Pipe({
  name: 'cityFilter'
})
export class CityFilterPipe implements PipeTransform {

  transform(list: UserView[], cityVaues: FilterItem[]): any {
    if (!this.hasToBeFiltered(cityVaues)) {
      return list;
    } else if (list) {
      return list.filter(item =>
        (cityVaues.filter(x => x.isChecked).map(x => x.value)).indexOf(item.city) > -1
      );
    }
    return [];
  }

  hasToBeFiltered(cityValues): boolean {
    return cityValues && cityValues.some(x => x.isChecked);
  }

}

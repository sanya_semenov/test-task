import { Component, OnInit, HostListener } from '@angular/core';
import { HttpService } from './shared/http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  constructor(private httpService: HttpService) {
    this.windowWidth = window.innerWidth;
  }
  title = 'app';
  windowWidth: number;
  @HostListener('window:resize', ['$event']) onWindowResize(event) {
    this.windowWidth = window.innerWidth;
  }

  ngOnInit() {

  }
}

import { User } from './user.model';

export class UserView {
  constructor(userObj?) {
    if (userObj) {
      this.user = userObj;
    }
  }
  _user: User;
  get user() {
    return this._user;
  }
  set user(value: User) {
    this._user = value;
    this.name = value.name;
    this.age = value.age;
    this.gender = value.gender;
    this.department = value.department;

    this.city = value.address.city;
    this.street = value.address.street;
  }
  get fullAddress() {
    return this.city + ', ' + this.street;
  }
  number: number;
  name: string;
  age: number;
  gender: string;
  department: string;
  city: string;
  street: string;
}

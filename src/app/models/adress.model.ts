import { Base } from './base';

export class Adress extends Base {
  constructor(json) {
    super();
    if (json) {
      this.city = this.MakeProp(json.city);
      this.street = this.MakeProp(json.street);
    }
  }
  city: string;
  street: string;
}

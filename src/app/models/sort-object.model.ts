export class SortObject {
  constructor(key, isDescending) {
    this.key = key;
    this.isDescending = isDescending;
  }

  key: string;
  isDescending: boolean;
  isTouched = false;
}

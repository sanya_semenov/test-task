import { Base } from './base';
import { Adress } from './adress.model';

export class User extends Base {
  constructor(json) {
    super();
    if (json) {
      this.id = this.MakeProp(json.id);
      this.name = this.MakeProp(json.name);
      this.age = this.MakeProp(json.age);
      this.gender = this.MakeProp(json.gender);
      this.department = this.MakeProp(json.department);
      this.address = new Adress(json.address);
    }
  }
  id: string;
  name: string;
  age: number;
  gender: string;
  department: string;
  address: Adress;
}

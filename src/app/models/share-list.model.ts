import { FilterItem } from './filter-item.model';

export class ShareList {
  constructor(list, isFromFilter) {
    this.list = list;
    this.isFromFilter = isFromFilter;
  }
  list: FilterItem[];
  isFromFilter: boolean;
}

export class FilterItem {
  constructor(value: string, isChecked: boolean) {
    this.value = value;
    this.isChecked = isChecked;
  }

  value: string;
  isChecked: boolean;
  matches: number;
}

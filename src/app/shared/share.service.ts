import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { FilterItem } from '../models/filter-item.model';
import { ShareList } from '../models/share-list.model';
import { UserView } from '../models/user-view.model';

@Injectable({
  providedIn: 'root'
})
export class ShareService {

  constructor() { }

  userList = new BehaviorSubject<UserView[]>([]);
  filteredList = new BehaviorSubject<UserView[]>([]);

  genderValues_ToFilter = new BehaviorSubject<FilterItem[]>([]);
  departmentValues_ToFilter = new BehaviorSubject<FilterItem[]>([]);
  cityValues_ToFilter = new BehaviorSubject<FilterItem[]>([]);

  genderValues_FromFilter = new BehaviorSubject<FilterItem[]>([]);
  departmentValues_FromFilter = new BehaviorSubject<FilterItem[]>([]);
  cityValues_FromFilter = new BehaviorSubject<FilterItem[]>([]);

  sendGenders(data) {
    this.genderValues_ToFilter.next(data);
  }

  sendDepartments(data) {
    this.departmentValues_ToFilter.next(data);
  }

  sendCities(data) {
    this.cityValues_ToFilter.next(data);
  }

  filterGenders(data) {
    this.genderValues_FromFilter.next(data);
  }

  filterDepartments(data) {
    this.departmentValues_FromFilter.next(data);
  }

  filterCities(data) {
    this.cityValues_FromFilter.next(data);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppConfig } from '../app.config';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private http: HttpClient,
    private config: AppConfig
  ) { }

  BASE_GetRequest(req, params?): Observable<any> {
    const queryParams = new HttpParams();
    if (params) {
      params.forEach(param => {
        queryParams.set(param.name, param.value);
      });
    }
    return this.http.get(req, { params: queryParams });
  }

  getUserList() {
    const reqString = this.config.isFakeAPI ? './assets/json/test_users.json' : 'domain.com/request';
    return this.BASE_GetRequest(reqString);
  }
}

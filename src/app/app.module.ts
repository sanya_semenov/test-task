import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppConfig } from './app.config';
import { HttpService } from './shared/http.service';
import { DashboardModule } from './modules/dashboard/dashboard.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    DashboardModule
  ],
  providers: [
    AppConfig,
    HttpService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
